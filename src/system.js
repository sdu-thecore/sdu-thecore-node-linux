const { readFile, readdir } = require('./fs');
const logger = require('./logger');

exports.getSerial = async () => {
  try {
    const cpuInfo = await readFile('/proc/cpuinfo', 'utf8');
    return cpuInfo
      .split('\n')
      .find(line => /^Serial/.test(line))
      .split(':')[1]
      .trim();
  } catch (err) {
    return '';
  }
};

exports.getActiveNetworkInterface = async () => {
  const kernelNetworkDir = '/sys/class/net';
  const ifaces = await readdir(kernelNetworkDir);
  const activeInterfaces = [];
  const promises = ifaces
    .filter(iface => iface !== 'lo')
    .map(async iface => {
      try {
        const linkStateFile = `${kernelNetworkDir}/${iface}/carrier`;
        const linkState = await readFile(linkStateFile, 'utf8');
        if (linkState.trim() === '1') {
          activeInterfaces.push(iface);
        }
      } catch (err) {
        logger.warn(`Reading interface status failed: ${iface}`);
      }
    });
  await Promise.all(promises);
  // The ".sort()" call will implicitly prefer eth (Ethernet) over wlan (Wi-Fi).
  return activeInterfaces.sort().shift() || 'none';
};

exports.getMacAddress = async iface => {
  try {
    const macAddressFile = `/sys/class/net/${iface}/address`;
    const macAddress = await readFile(macAddressFile, 'utf8');
    return macAddress.trim();
  } catch (err) {
    logger.warn(`Reading mac address failed: ${iface}`);
    return '';
  }
};

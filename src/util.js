exports.delay = ms =>
  new Promise(resolve => {
    const timerId = setTimeout(() => {
      clearTimeout(timerId);
      resolve();
    }, ms);
  });

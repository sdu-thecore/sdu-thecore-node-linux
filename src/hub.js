const socketio = require('socket.io-client');
const dotenv = require('dotenv');
const logger = require('./logger');

dotenv.config();

let socket = null;

exports.connect = token => {
  if (!socket) {
    socket = socketio(process.env.HUB_URL, {
      transportOptions: {
        polling: {
          extraHeaders: {
            authorization: `Bearer ${token}`
          }
        }
      }
    });

    socket.on('connect', () => {
      logger.info(`Connected to hub: ${process.env.HUB_URL}`);
    });

    socket.on('disconnect', reason => {
      logger.warn(`Disconnected from hub: ${reason}`);
    });
  }

  return socket;
};

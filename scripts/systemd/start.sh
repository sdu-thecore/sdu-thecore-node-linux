#!/usr/bin/env bash

# Load node version manager.
source "${HOME}/.nvm/nvm.sh"

# Start the application.
node src/index.js

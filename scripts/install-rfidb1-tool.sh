#!/usr/bin/env bash

# Global variables.
artifactDownloadLink="https://eccel.co.uk/wp-content/downloads/rfidb1-tool.zip"
tempDir="/tmp/rfidb1-tool"

# Color variables.
default='\e[00;0m'
red='\e[00;31m'
green='\e[00;32m'
magenta='\e[00;35m'

# Color shortcuts.
d=${default}
r=${red}
g=${green}
m=${magenta}

# Message indicators.
info="${g}info:${d}   "
error="${r}error:${d}  "

# Check if user has sudo rights.
if [[ ${EUID} -ne 0 ]]; then
  echo -e "${error}This script must be run as ${m}root${d} or with ${m}sudo${d}."
  exit 1
fi

# Display error and exit program in case a command failed.
# First parameter is the return code of a program,
# which can be obtained by $? and the second parameter
# is a message in case the return code is non-zero.
# Usage: check_error $? "Something went wrong."
check_error() {
  if [ ${1} -ne 0 ]; then
    echo -e "${error}${2}"
    exit 1
  fi
}

# Create temporary directory.
echo -e "${info}Creating temporary directory: ${m}${tempDir}${d}"
mkdir -p "${tempDir}"
pushd "${tempDir}"

# Download CLI package from hardware vendor.
echo -e "${info}Downloading artifact from vendor page..."
wget -nv -O artifact.zip ${artifactDownloadLink}
check_error $? "Downloading install script failed."

echo -e "${info}Installing development dependencies: ${m}unzip libmcrypt-dev${d}"
apt-get update -y
apt-get install -y unzip libmcrypt-dev

# Unzip downloaded archive.
echo -e "${info}Extracting artifact..."
unzip -u artifact.zip

# Install RFID command line interface.
pushd rfidb1*

echo -e "${info}Compiling binary: ${m}rfidb1-tool${d}"
make

echo -e "${info}Installing binary: ${m}rfidb1-tool${d}"
make install

popd

# Uninstall development dependencies.
echo -e "${info}Removing development dependencies: ${m}unzip libmcrypt-dev${d}"
apt-get purge -y --autoremove unzip libmcrypt-dev

# Clean up temporary files.
popd
rm -rf "${tempDir}"

# Indicate successful installation.
echo -e "${info}Successfully installed dependency from source: ${m}rfidb1-tool${d}"
